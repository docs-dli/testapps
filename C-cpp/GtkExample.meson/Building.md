## Building this project
you will need meson, gtk4 and ninja

```
# For building
meson setup build && meson compile -C build

# For running
./build/main
```