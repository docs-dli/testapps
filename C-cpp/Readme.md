# C++ C Applications
This repository is dedicated to show how to publish various types of C and C++ applications to Higenku

## Applications
- HelloWolrd:               A simple Console App using gcc and a custom build script

- GtkExample.make:          A simple Gtk App using make
- GtkExample.meson:         A simple Gtk App using meson
- GtkExample.cmake:         A simple Gtk App using cmake

- QtExample.make:           A simple Qt App using make
- QtExample.cmake:          A simple Qt App using cmake

