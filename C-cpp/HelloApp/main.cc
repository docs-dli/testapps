#include "CoolClass.hh"

#include "iostream"

int main()
{
    CoolClass *e;
    e->speak();
    e->walk();

    return 0;
}