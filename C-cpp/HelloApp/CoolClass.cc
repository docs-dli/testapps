#include "CoolClass.hh"
#include "iostream"

#define Log(x) std::cout << x << std::endl;


CoolClass::CoolClass()
{
    Log("New Person Created");
}

CoolClass::~CoolClass()
{
    Log("Destroying new Person");
}
void CoolClass::speak()
{
   Log("Hello I am a new Person");
}

void CoolClass::walk()
{
    Log("I am walking");
}
