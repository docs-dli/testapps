# Python Applications
This repository is dedicated to show how to publish various types of Python applications to Higenku

## Applications
- ConsoleApp:   Python simple console app
- GtkApp:       Python simple desktop app using gtk
- PyQtApp:      Python simple desktop app using qt
