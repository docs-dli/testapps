cmake_minimum_required (VERSION 2.8)

find_package(Java REQUIRED)
include(UseJava)

project(app NONE)

# set(CMAKE_JAVA_COMPILE_FLAGS "-source" "1.6" "-target" "1.6")

add_jar(app App.java)

get_target_property(_jarFile app JAR_FILE)
get_target_property(_classDir App CLASSDIR)

message(STATUS "Jar file ${_jarFile}")
message(STATUS "Class compiled to ${_classDir}")