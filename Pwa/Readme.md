# Pwa Applications

This repository is dedicated to show how to publish various types of Pwa applications to Higenku

## Applications
- HelloWolrd:     A simple vanila app
- TakeControl:    An Application with filesystem, notification and dialogs capeabilities
- Blazor Pwa:	  A simple Blazor Pwa Using Pwa Packager
- Svelte.pwa:     A simple Pwa App using typescript and svelte 
- Angular.pwa:    A simple Pwa app using typescript and angular 
- Vue.pwa:        A simple Pwa app using typescript and vue 
- React.pwa:      A simple Pwa app using typescript and react 
